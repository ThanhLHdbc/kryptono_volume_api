"""
API to get ticker data and volume data in 24h from 
Kryptono Exchange

Author: BC, Krypto
"""
import sys
import time
import pandas as pd
import socket
import websocket

from decimal import Decimal

column_legend = {'s': 'Symbol',
           'c': 'Close Price',
           'ch': 'Price Change',
           'h': 'High Price',
           'l': 'Low Price',
           'n': 'Now Price (current)',
           'o': 'Open Price',
           'pc': 'Percent Change',
           'q': 'Quantity',
           't': 'Timestamp'}

DEFAULT_WEBSOCKET_URL = "wss://engine.kryptono.exchange/ws/v1/tk"
MAX_RETRY = 3

class Kryptono:
    websocket_url = None
    base_symbol_list = None
    dataframe_ticker24h = None
    dataframe_volume24h = None

    def __init__(self, websocket_url=DEFAULT_WEBSOCKET_URL):
        self.websocket_url = websocket_url
        self. base_symbol_list = {'_BTC':list(),
                                  '_ETH':list(),
                                  '_KNOW':list(),
                                  '_USDT':list()}

        self.get_ticker24h()

    def get_ticker24h(self):
        ws = None

        retry = 0
        while True:    
            try:
                ws = websocket.create_connection(self.websocket_url)
                break
            except socket.error:
                pass  # socket hasn't started listening yet.
            except websocket.WebSocketException as err:
                if "Handshake Status 404" in str(err):
                    pass  # socket is listening, but hasn't set up the /sc2api endpoint yet.
                else:
                    pass  # other exceptions
            retry += 1
            if retry >= MAX_RETRY:
                sys.exit("Failed to create the socket.") 
            time.sleep(3)


        for i in range(2):
            result =  ws.recv()

        ws.close()
        
        self.dataframe_ticker24h = pd.DataFrame(eval(result)["d"])

        return True


    def get_base_symbol_list(self):
        """ Get the list of symbols come in pair with the base_symbols
        BTC, ETH, KNOW and USDT
        """
        df = self.dataframe_ticker24h.set_index('Symbol', drop=False)
  
        for symbol in df.index.get_values():
          if '_BTC' in symbol and symbol not in self.base_symbol_list['_BTC']:
            self.base_symbol_list['_BTC'].append(symbol)
          if '_ETH' in symbol and symbol not in self.base_symbol_list['_ETH']:
            self.base_symbol_list['_ETH'].append(symbol)
          if '_KNOW' in symbol and symbol not in self.base_symbol_list['_KNOW']:
            self.base_symbol_list['_KNOW'].append(symbol)
          if '_USDT' in symbol and symbol not in self.base_symbol_list['_USDT']:
            self.base_symbol_list['_USDT'].append(symbol)


    def get_volume24h_engine(self, base_symbol):
        """ Get volume24h of base_symbol from engine

        base_symbol: string of base currency. ('BTC', 'ETH', 'KNOW', 'USDT')
        """
        self.get_base_symbol_list()

        df = self.dataframe_ticker24h.set_index('Symbol', drop=False)

        total = Decimal(0)

        ddf = df.loc[:,'Quantity']
        for pair_symbol in self.base_symbol_list['_' + base_symbol]:
            total += Decimal(ddf[pair_symbol])

        return total

    def get_dataframe_ticker24h(self):
        """ Get ticker24h data and replace convenient legend for data columns
        """
        self.dataframe_ticker24h.rename(columns=column_legend, inplace=True)

    def get_dataframe_volume24h(self):
        """ Get volume24h data, calculated in amount of BTC, ETH, KNOW and USDT
        """
        volume24h = dict()
        for base_symbol in self.base_symbol_list.keys():
            volume24h.update({base_symbol.replace('_','') : self.get_volume24h_engine(base_symbol.replace('_',''))})

        self.dataframe_volume24h = pd.DataFrame(list(volume24h.items()), columns=['Symbol','Volume24h'])

    def export_ticker24h_to_csv(self, filename='kryptono_ticker24h.csv'):
        """ export ticker24h data of exchange pairs to CSV file

        filename:
        """
        self.get_dataframe_ticker24h()

        df = self.dataframe_ticker24h.set_index('Symbol')
        df.to_csv(filename, sep=',', encoding='utf-8')

    def export_volume24h_to_csv(self, filename='kryptono_volume24h.csv'):
        """ export volume24h data of (BTC, ETH, KNOW, USDT) to CSV file

        filename: 
        """
        self.get_dataframe_volume24h()
        df = self.dataframe_volume24h.set_index('Symbol')

        df.to_csv(filename, sep=',', encoding='utf-8')

    def get_volume24h(self, base_symbol):
        """ Retrieve volume24h of a currency base_symbol

        base_symbol: string of base currency. ('BTC', 'ETH', 'KNOW', 'USDT')
        """
        return self.get_volume24h_engine(base_symbol)

    def get_ticker24h_pair(self, pair_symbol):
        """ Retrieve ticker24h of a pair

        pair_symbol: string of the exchance pair (e.g., 'ETH_BTC')
        """
        df = self.dataframe_ticker24h.set_index('Symbol')

        return df.loc[pair_symbol, : ]

