# Kryptono Volume API

This is Python wrapper for Kryptono Exchange volume 24h. If you reach here looking for [Kryptono Exchange](https://kryptono.exchange") to purchase cryptocurrencies, please go to https://kryptono.exchange

Make sure you update often and check the Changelog for new features and bug fixes.

##Features

* Retrieve ticker24h of the exchange pairs in Kryptono Exchange
* Retrieve volume24h of the base currencies in Kryptono Exchange
* Export data to CSV formatted files


##Prerequisites
This code works with [Python 3](https://www.python.org/downloads/release/python-365/) and use:

* [pandas](https://pandas.pydata.org/)

It is recommended to use the latest version of Python and required libraries.

##Installation
Copy Kryptono.py to the working folder and import the Kryptono library.

```python
    from Kryptono import Kryptono
```

By default the link to the Kryptono Exchange has been provided in the library. However, in case of update, you can input the new link when initiate the Kryptono class, as following:


```python
    kr = Kryptono(websocket_url="wss://new_link_to.kryptono.exchange/ws/v1/tk")
    # ...
```

##Description
1. Create a new Kryptono class, e.g., ```kr = Kryptono()```
2. ```export_ticker24h_to_csv(csv_filename)```: Export the ticker24h data of every exchange pair to a CSV file. By default, the output filename is ```kryptono_ticker24h.csv```
3. ```export_volume24h_to_csv(csv_filename)```: Export the volume24h data of tuple (BTC, ETH, KNOW, USDT) to a CSV file. By default, the output filename is ```kryptono_volume24h.csv```
4. ```get_volume24h(base_currency)```: Get the volume24h data of a base currency BTC, ETH, KNOW, or USDT. 
    * ```base_currency```: string represents the currency: 'ETH', 'BTC', 'KNOW', 'USDT'
5. ```get_ticker24h_pair(pair)```: Get the ticker24h data of an exchange pair. 
    * ```pair```: string represents the pair, with underscore '_' goes between two currency symbols, e.g., 'ETH_BTC', 'GTO_KNOW'

####Ticker 24h sample

|Symbol|Close Price|Price Change|High Price|Low Price|Now Price (current)|Open Price|Percent Change|Quantity |Timestamp|
|:----|----:|----:|----:|----:|----:|----:|----:|----:|----:|
|**ETH_BTC** |0.08367236|-0.00064540|0.08543800|0.08242882|0.08358869|0.08346487|-0.76619810|14.15102244|1526976000000|
|**KNOW_ETH**|0.00013569| 0.00000000|0.00014722|0.00013478|0.00014716|0.00014716| 0.00000000|13.91787656|1526976000000|
|------|------|------|------|------|-----|-----|-------|------|--------|


####Volume 24h sample

|Symbol|Volume24h|
|:---- |----:|
|**BTC**   |    72.07919878|
|**ETH**   |  1524.04316456|
|**KNOW**  |269498.25508959|
|**USDT**  |258481.93554476|

##Running the sample code
The sample code in get_volume.py illustrates how to use this API. Copy this file and Kryptono.py into one folder. Run it by:

```
    $python get_volume.py
```

__NOTE__: If you get error running the sample code, it is often caused by missing required libraries. Please check the __Prerequisites__.


##Author

* BC - Krypto

##Feedback
If you have any suggestion or find any issue with this library, please contact us via [Kryptono Exchange](https://kryptono.zendesk.com/hc/en-us).
We appreciate your support and contribution.
