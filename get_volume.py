""" 
Sample code to retrieve Kryptono ticker data and volume in 24h
"""

from Kryptono import Kryptono

kr = Kryptono()

kr.export_ticker24h_to_csv()
kr.export_volume24h_to_csv()

print(kr.dataframe_ticker24h)
print('=================')
print('ETH', kr.get_volume24h('ETH'))
print('BTC', kr.get_volume24h('BTC'))
print('KNOW', kr.get_volume24h('KNOW'))
print('USDT', kr.get_volume24h('USDT'))

print('=================')
pair = 'ETH_BTC'
print(f'{pair}:\n{kr.get_ticker24h_pair(pair).to_string()}')